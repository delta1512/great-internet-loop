# Great Internet Loop

This is a distributed project that I've always wanted to make. I hope that one day this will become a BOINC project and we can find the largest hyperlink loop.

# Rules

A hyperlink loop is an ordered set of hyperlinks such that the very last hyperlink serves a web page which contains a hyperlink which is equal to the first one in the ordered set.

A non-trivial hyperlink loop is an ordered set of more than 2 hyperlinks such that every domain name in each link is unique and the domain names of every hyperlink (except for the first) can be found in the HTML body of the previous hyperlink.

A valid hyperlink is one that is not a domain name of a search engine, nor does it have the same domain (or sub-domain) as any other hyperlink in the set.

The aim is to find the largest loop provided these rules.

# Rationale

It's kinda fun.
