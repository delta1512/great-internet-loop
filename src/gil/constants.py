# Sourced from https://en.wikipedia.org/wiki/List_of_search_engines
BLACKLISTED_SITES = (
    'ask.com', 'baidu.com', 'dogpile.com', 'duckduckgo.com',
    'ecosia.org', 'excite.com', 'gigablast.com', 'google.com',
    'hotbot.com', 'lycos.com', 'metacrawler.com', 'bing.com',
    'mojeek.com', 'mojeek.co.uk', 'petalsearch.com', 'qwant.com',
    'searx.me', 'sogou.com', 'startpage.com', 'swisscows.com',
    'webcrawler.com', 'search.yahoo.com', 'yandex.com',
    'yippy.com'
)

URL_REGEX = '''^(http(s)?://)?[\-a-zA-Z0-9\._]{1,256}\.[a-zA-Z0-9]{1,6}([\-a-zA-Z0-9_\./\?\=]*)$'''
DOMAIN_REGEX = '''[-a-zA-Z0-9@%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}'''
