from gil import util
from gil import client


'''
Backtracking state:
    A list of lists where each internal list is as follows:
        [CURRENT_URL, [LIST, OF, CONTAINED, URLS]]

    Where the list of contained URLs are possible valid next URLs in the loop.
'''


def backtrack_next_step(state: list, recursion_limit: int):
    '''
    :returns: list - The new state
    '''
    # Shallow copy list
    cw_state = list(state)
    # First check if we are at our limit
    if len(cw_state) > recursion_limit:
        cw_state.pop()

    # Structured as [URL, [LIST, OF, URLs, WITHIN, URL]]
    current_state = cw_state.pop() # We will return this to the list later

    # If there is at least 1 more branch, keep going
    if len(current_state[1]) > 0:
        next_url = current_state[1][-1] # The last URL in the list to branch to
        # Replace the popped state without the extracted url
        cw_state.append([current_state[0], current_state[1][:-1]])

        try:
            # Perform the fetch and add the next state
            cw_state.append([
                next_url,
                client.get_all_branches_from_url(
                    next_url,
                    util.backtrack_state_to_url_list(cw_state)
                )
            ])
        except AssertionError:
            # If a HTTP error occurs, then return what we have
            # The erroneous url is already cut out above
            return cw_state

        return cw_state
    else:
        # Simply leave it popped and call it a day
        return cw_state
