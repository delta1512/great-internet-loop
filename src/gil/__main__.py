import sys
import time
import getopt
import logging

from gil import util
from gil import algo
from gil import client


# TEMP
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


HELP = '''The Great Internet Loop (GIL) app

Program that attempts to find the largest loop of hyperlinks on the internet.
Provided a starting URL, this programm will perform recursive backtracking on
subsequent hyperlinks that are found in the previous link.

If a program state is detected in the current working directory, then that state
is loaded.

OPTIONS:
    -h --help
        Help. Print this message and exit.

    -d MAX_DEPTH
        The maximum recursion depth for the program to follow. This in-effect
        puts a limit on the maximum size of the loop but prevents
        endless recursion so that other links can be tried. Default is 100.

    -l MAX_LENGTH
        The maximum number of steps the program will go through. One step is
        either adding a node to the state or popping a node from the state.
        Default is 100.

    -s START_URL
        The URL that the program will start searching from.

AUTHOR:
    Written by Marcus Belcastro

SEE ALSO:
    Source code: https://gitlab.com/delta1512/great-internet-loop
'''


def main(start: str, state: list, best: list, max_recursion: int, max_steps: int=99999999, checkpoint_interval: int=60):
    last_checkpoint = 0

    if len(state) == 0:
        # Build the first state from the starting state
        state.append([start, client.get_all_branches_from_url(start, [])])

    for i in range(max_steps):
        logging.debug('Current state: %s', state)
        new_state = algo.backtrack_next_step(state, max_recursion)
        logging.debug('New state: %s', new_state)

        # If we popped a state, then no request was made and we can go again
        if len(new_state) < len(state):
            state = new_state
            continue

        state = new_state

        # Check for new best
        if util.is_loop(state):
            # Remove any instances of the start URL in the next URLs
            state[-1][1] = list(filter(
                lambda u: not util.is_subdomain(
                    util.get_domain_from_url(start_url),
                    util.get_domain_from_url(u)
                ), state[-1][1]
            ))
            new_best = util.backtrack_state_to_url_list(state)

            if (len(new_best) > len(best)):
                logging.info('Found a new best, storing state...')
                best = new_best
                util.store_state(state, best)

        if (last_checkpoint % checkpoint_interval == 0):
            logging.info('Checkpoint hit, storing state...')
            util.store_state(state, best)

        time.sleep(1)

        last_checkpoint += 1

    util.store_state(state, best)


if __name__ == '__main__':
    start_url = ''
    max_depth = 100
    max_len = 100

    opts, args = getopt.getopt(sys.argv[1:], 'hl:s:d:', ['help'])

    for opt, arg in opts:
        if opt == '-h' or opt == '--help':
            print(HELP)
            sys.exit(0)
        elif opt == '-l':
            max_len = int(arg)
        elif opt == '-s':
            start_url = arg
        elif opt == '-d':
            max_depth = int(arg)

    assert util.is_valid_url(start_url), 'Start URL is not valid'

    backtrack_state, best_state = util.retrieve_state()

    main(
        start_url,
        backtrack_state,
        best_state,
        max_recursion=max_depth,
        max_steps=max_len
    )

    sys.exit(0)
