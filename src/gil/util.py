import re
import os
import csv

import tldextract

from gil.constants import *


def retrieve_state(backtrack_file: str='backtrack.csv', best_file: str='best.csv'):
    backtrack_state = []
    best_state = []

    if os.path.exists(backtrack_file):
        with open(backtrack_file, 'r') as csvf:
            reader = csv.reader(csvf, delimiter=',', quotechar='"')

            for ln in reader:
                cont_urls = ln[1].split(',')

                if cont_urls == ['']:
                    cont_urls = []

                backtrack_state.append([ln[0], cont_urls])

            # Get rid of the titles
            backtrack_state.pop(0)

    if os.path.exists(best_file):
        with open(best_file, 'r') as csvf:
            reader = csv.reader(csvf, delimiter=',', quotechar='"')

            for ln in reader:
                best_state.append(ln[0])

            # Get rid of the titles
            best_state.pop(0)

    return backtrack_state, best_state


def store_state(backtrack_state: list, best_state: list, backtrack_file: str='backtrack.csv', best_file: str='best.csv'):
    with open(backtrack_file, 'w') as csvf:
        writer = csv.writer(csvf, delimiter=',', quotechar='"')

        writer.writerow(['Current', 'Branches'])

        for ln in backtrack_state:
            writer.writerow([ln[0], ','.join(ln[1])])


    with open(best_file, 'w') as csvf:
        writer = csv.writer(csvf, delimiter=',', quotechar='"')

        writer.writerow(['Loop'])

        # Only non-trivial loops are saved
        if len(best_state) > 2:
            for ln in best_state:
                writer.writerow([ln])


def is_valid_url(url: str):
    # First match regex for a link
    if not re.match(URL_REGEX, url):
        return False

    # Then filter out all search engines
    domain = get_domain_from_url(url)

    if (not bool(domain)) or (domain in BLACKLISTED_SITES):
        return False

    return True


def get_domain_from_url(url: str):
    sr = re.search(DOMAIN_REGEX, url)
    if not sr:
        return ''

    return url[sr.span()[0]:sr.span()[1]]


def backtrack_state_to_url_list(backtrack_state: list):
    '''
    Converts the list of lists of the backtrack state to simply an ordered list
    of urls that have currently been iterated.
    '''
    final = []

    for s in backtrack_state:
        final.append(s[0])

    return final


def is_subdomain(url_a: str, url_b: str):
    '''
    Returns True if the domain url_a is equal to or a sub-domain of url_b
    '''
    dom_a = tldextract.extract(url_a)
    dom_b = tldextract.extract(url_b)

    return dom_a.domain == dom_b.domain


def is_loop(state: list):
    '''
    Function that returns whether or not a current backtrack state is a
    hyperlink loop or not.

    :param state: A backtracking state
    '''
    target = state[0][0]
    next_urls = state[-1][1] # Get the possible next URLs

    for u in next_urls:
        if u == target:
            return True

    return False
