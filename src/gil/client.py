import re
import random
import logging
import requests

import bs4

from gil import util


def get_page_html(url: str):
    '''
    :param url: The URL to extract HTML from
    :returns: str - The HTML of the webpage provided
    :raises: AssertionError - If the HTTP status code is not 200 or timed-out
    '''
    headers = requests.utils.default_headers()
    headers['User-Agent'] += '- Great Internet Loop project (https://gitlab.com/delta1512/great-internet-loop)'

    try:
        r = requests.get(url, headers=headers, timeout=5)
    except (requests.exceptions.RequestException) as e:
        logging.error('Failed to fetch webpagewith error: %s', e)
        assert False, 'Failed to fetch webpage'

    assert r.status_code == 200, 'Failed to fetch webpage, returned HTTP {}'.format(r.status_code)
    assert 'text/html' in r.headers.get('content-type', ''), 'Wrong MIME type found, skipping'

    return r.text


def scrape_valid_hyperlinks(src_html: str):
    '''
    Fetches a list of all the valid hyperlinks in the provided webpage.
    :param src_html: The raw string of the HTML webpage
    :returs: list - The list of valid hyperlinks in the page
    '''
    final = []
    # First convert to beautiful soup and extract the links
    bs = bs4.BeautifulSoup(src_html, 'html.parser')

    # For when the website doesn't have a body
    if bs.body is None:
        return []

    body_links = bs.body.find_all('a')

    for link in body_links:
        link = link.get('href', '')

        if not util.is_valid_url(link):
            continue

        final.append(link)

    random.shuffle(final) # Randomize the output to improve variation

    return final


def get_all_branches_from_url(from_url: str, prev_urls: list):
    '''
    :param from_url: The URL to scan for branching hyperlinks on.
    :param prev_urls: The list of URLs that are currently in the loop so that we
        can cull already seen domains.
    '''
    final = []
    is_start = len(prev_urls) == 0
    from_url_domain = util.get_domain_from_url(from_url)
    links = scrape_valid_hyperlinks(get_page_html(from_url))

    if is_start:
        init_url = ''
    else:
        init_url = util.get_domain_from_url(prev_urls[0])

    for l in links:
        link_domain = util.get_domain_from_url(l)
        # If this turns into a hyperlink loop, add it and continue
        if init_url == l:
            final.append(l)
            continue

        # Check that the link is not the same as or sub-domain of current URL
        if util.is_subdomain(from_url_domain, link_domain):
            continue

        valid = True

        # Check that the domain isn't found in any previous url (except the first)
        for u in prev_urls[1:]:
            url_domain = util.get_domain_from_url(u)
            valid = (valid) and (not util.is_subdomain(link_domain, url_domain))

            if not valid:
                break

        if valid:
            final.append(l)

    return final
